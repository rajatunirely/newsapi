import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NewFormComponent } from './components/new-form/new-form.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchFeedComponent } from './components/search-feed/search-feed.component';

@NgModule({
  declarations: [
    AppComponent,
    NewFormComponent,
    SearchFeedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
