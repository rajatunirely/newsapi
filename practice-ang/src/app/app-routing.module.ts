import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NewFormComponent } from './components/new-form/new-form.component';
import { SearchFeedComponent } from './components/search-feed/search-feed.component';

const routes: Routes = [
    { path: '', component: NewFormComponent },
    { path: 'search-feed', component: SearchFeedComponent },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule { }
