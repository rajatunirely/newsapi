import { Component, OnInit, ViewChild } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { searchQueryResponse } from 'src/app/models/search.model';
import { SearchServiceService } from 'src/app/services/search-service.service';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.css']
})
export class NewFormComponent implements OnInit {

  constructor(
      private fb: FormBuilder,
      private searchService: SearchServiceService,
      private router: Router,
      ) { }

  profileForm = this.fb.group({
    searchQuery: ['', Validators.required],
    sources: this.fb.group({
        abcnews: '',
        bbcnews: '',
        bbcsport: '',
        espn: '',
        businessinsider: '',
        buzzfeed: '',
        cnbc: '',
        cnn: ''
    }),
  });

  get searchQuery() { return this.profileForm.get('searchQuery'); }

  ngOnInit(): void {
    this.searchService.searchData$.subscribe((res: searchQueryResponse) => {
        if(res?.totalResults) {
            this.router.navigate(['search-feed']);
        }
    });
  }

  onSubmit(){
    let newSources = Object.assign({}, this.profileForm.value.sources);
    let querySources = []; 
    for(let source in newSources) {
        if(newSources[source]) {
            querySources.push(newSources[source]);
        }
    }
    this.searchService.getSearchResult(this.profileForm.value.searchQuery, querySources);
}

}
