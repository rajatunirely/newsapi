import { Component, OnInit } from '@angular/core';
import { articleResponse, searchQueryResponse } from 'src/app/models/search.model';
import { DatabaseServiceService } from 'src/app/services/database-service.service';
import { SearchServiceService } from 'src/app/services/search-service.service';

@Component({
  selector: 'app-search-feed',
  templateUrl: './search-feed.component.html',
  styleUrls: ['./search-feed.component.css']
})
export class SearchFeedComponent implements OnInit {

    newsFeed : articleResponse[] = [];

    currentPage : number = 0;
    currentFeed : articleResponse[] = [];
    paginators : number;
    contentPerPage: number = 5;

    increasePaginator: boolean = true;
    decreasePaginator: boolean = false;

    constructor(
        private searchService: SearchServiceService,
        private databaseService: DatabaseServiceService
    ) { }

    ngOnInit(): void {
        this.searchService.searchData$.subscribe((res: searchQueryResponse) => {
            if(res?.articles){

                console.log(res.articles);
                this.newsFeed = res.articles;   
                
                this.paginators = Math.ceil((this.newsFeed.length) / this.contentPerPage);
                this.currentFeed = this.newsFeed.slice(this.currentPage * this.contentPerPage, (this.currentPage + 1) * this.contentPerPage);
            }
        });

        this.databaseService.saveToDatabase(this.newsFeed);

    }

    PaginatorIncrease() {
        this.decreasePaginator = true;
        this.currentPage += 1;

        this.currentFeed = this.newsFeed.slice(this.currentPage * this.contentPerPage, (this.currentPage + 1) * this.contentPerPage);
        
        if( this.currentPage + 1 >= this.paginators ){
            this.increasePaginator = false;
        }
    }

    PaginatorDecrease() {
        this.increasePaginator = true;
        this.currentPage -= 1;

        this.currentFeed = this.newsFeed.slice(this.currentPage * this.contentPerPage, (this.currentPage + 1) * this.contentPerPage);

        if( this.currentPage -1 < 0) {
            this.decreasePaginator = false;
        }
    }

}
