export interface searchQueryResponse{
    status: string,
    totalResults: number,
    articles: articleResponse[]
};

export interface articleResponse{
    source: { id: string, name: string},
    author: string,
    title: string,
    description: string,
    url: string,
    urlToImage: string,
    publishedAt: string,
    content: string,
}