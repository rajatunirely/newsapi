import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { searchQueryResponse } from '../models/search.model';

@Injectable({
  providedIn: 'root'
})
export class SearchServiceService {
    readonly NEWS_API;
    readonly API_KEY;

    searchData$: BehaviorSubject<searchQueryResponse> = new BehaviorSubject<searchQueryResponse>(null);

    constructor(
        private http: HttpClient
    ) {
            this.NEWS_API = environment.NEWS_API;
            this.API_KEY = environment.NEWS_API_KEY;
    }

    getSearchResult(searchQuery: String, sources: any[]){
        let api_url: String = '';
        let sourcesStr = sources.join(',');
        api_url += this.NEWS_API;

        if(sources.length) {
            api_url += `/everything?q=${searchQuery}&sources=${sourcesStr}`;
        } else {
            api_url += `/top-headlines?q=${searchQuery}`;
        }
        api_url += `&pageSize=10&apiKey=${this.API_KEY}`;
        this.http.get<searchQueryResponse>(`${api_url}`)
            .subscribe((res: searchQueryResponse) => {
                if(res) {
                    this.searchData$.next(res);
                    }
            });
        }
}

