import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { articleResponse } from '../models/search.model';

@Injectable({
  providedIn: 'root'
})
export class DatabaseServiceService {

    readonly SERVER_URL;

    constructor(
        private http: HttpClient
    ) { 
        this.SERVER_URL = environment.SERVER_URL;
    }

    getDatabaseResult() {
        this.http.get(`${this.SERVER_URL}/search-result`).subscribe((res) => console.log(res));
    }

    saveToDatabase(payload: articleResponse[]){
        this.http.post(`${this.SERVER_URL}/search-result/save`, {payload})
            .subscribe((res) => {
                if(res) {
                    console.log("saved");
                }
            });
    }

}
