const Newtasks = require("../config/models/newTask");
const SearchResult = require("../config/models/searchResult");

module.exports = {

    getResult: (req, res, next) => {

        SearchResult.find({})
            .then((list) => res.send(list))
            .catch((err) => console.log(err));
    },

    saveResult: (req, res, next) => {
        let { payload } = req.body;
        let uniqueContent = [];

        payload.forEach( async (article) => {

            let content = await SearchResult.find({ author: article.author, title: article.title});
            
            if ( content.length) return;

            uniqueContent.push(content);
            (new SearchResult({
                'source': article.source,
                'author': article.author,
                'title': article.title,
                'description': article.description,
                'url': article.url,
                'urlToImage': article.urlToImage,
                'publishedAt': article.publishedAt,
                'content': article.content
            }))
            .save()
            .then((res) => console.log("done"))
            .catch((err) => console.log(err));
        });

        res.json("ok");
    }

}