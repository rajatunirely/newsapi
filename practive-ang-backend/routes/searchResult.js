const express = require('express');
const { saveResult, getResult,  } = require('../controllers/searchResult');
const { findUniqueArticles } = require('../middleware/searchResult');
const router = express.Router();

router.get('/', getResult);

router.post('/save', saveResult);

module.exports = router;