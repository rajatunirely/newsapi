var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const express_graphql = require('express-graphql');

const mongoose = require('./config/mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var searchResRouter = require('./routes/searchResult');
const mySchema = require('./graphQL/schema');
const resolvers = require('./graphQL/resolvers');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    next();
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const root = resolvers;

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/search-result', searchResRouter);
app.use('/graphql', express_graphql.graphqlHTTP({
  schema: mySchema,
  rootValue: root,
  graphiql: true
}))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(process.env.PORT || 3000, () => {
    console.log("server connected");
})

module.exports = app;

console.log("develop new");
console.log("master new");
