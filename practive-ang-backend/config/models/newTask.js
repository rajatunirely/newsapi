const mongoose = require('mongoose');

const newTaskSchema = new mongoose.Schema({
    title: {
        type: String,
        minlength: 3,
    },
    list: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    },
    complete: {
        type: Boolean,
        default: false
    }
});

const Newtasks = mongoose.model('newtasks', newTaskSchema);
module.exports = Newtasks;